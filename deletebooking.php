<?php
	include('functions.php');
	redirectHTTPS();
	checkCookies();
	session_start();

	// If the user is not authorized, print an error message
	if(! checkSessionValidity()) {
		$error = "<div class=\"container-fluid bg-danger text-warning\"><h4>You must be logged to access this page.</h4></div>";
		goto displayPage;
	}

	// If the user is not booked, print an error message
	if(! bookedUser($_SESSION[$SESSION_PREFIX . 'username'])) {
		$error = "<div class=\"container-fluid bg-danger text-warning\"><h4>You have no active booking yet. Please, make one to access this page.</h4></div>";
		goto displayPage;
	}

	$connection = connect();

	try {
		mysqli_autocommit($connection, false);
		mysqli_begin_transaction($connection);

		$statement = mysqli_stmt_init($connection);
		mysqli_stmt_prepare($statement, "DELETE FROM `booking` WHERE `username` = ?");
		mysqli_stmt_bind_param($statement, 's', $user);
		$user = $_SESSION[$SESSION_PREFIX . 'username'];
		mysqli_stmt_execute($statement);
		mysqli_stmt_close($statement);

		// Clean unused cities
		$cities = array();
		$result = query($connection, "SELECT `city` FROM `shuttle`");
		if($result == false)
			throw new Exception();
		while(($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) != NULL) {
			$city = $row['city'];
			$cities[$city] = $city;
		}
		mysqli_free_result($result);

		$bookedCities = array();
		$result = query($connection, "SELECT `departure`, `arrival` FROM `booking` FOR UPDATE");
		if($result == false)
			throw new Exception();
		while(($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) != NULL) {
			$city = $row['departure'];
			$bookedCities[$city] = $city;
			$city = $row['arrival'];
			$bookedCities[$city] = $city;
		}
		mysqli_free_result($result);

		$deletableCities = array();
		foreach($cities as $city) {
			if(! isset($bookedCities[$city])) // If city is not booked, it is not needed to pass through
				$deletableCities[$city] = $city;
		}

		$statement = mysqli_stmt_init($connection);
		mysqli_stmt_prepare($statement, "DELETE FROM `shuttle` WHERE `city` = ?");
		mysqli_stmt_bind_param($statement, 's', $index);

		foreach($deletableCities as $index => $city) {
			if(! mysqli_stmt_execute($statement))
				throw new Exception();
		}
		mysqli_stmt_close($statement);
		mysqli_commit($connection);
		$_SESSION[$SESSION_PREFIX . 'message'] = "<div class=\"container-fluid bg-success text-info\"><h4>Your booking has been deleted.</h4></div>";
		header("location: index.php");
	} catch (Exception $e) {
		// Some error occurred
		mysqli_rollback($connection);
		$error = "<div class=\"container-fluid bg-danger text-warning\"<h4>Some error occurred! Please, retry.</h4></div>";
	}
	mysqli_autocommit($connection, true);
	close($connection);

	displayPage:
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="author" content="Enrico Franco">
	<title>Delete booking</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="bootstrap/css/bootstrap.css">
</head>
<body>
<?php
	include('header.php');
?>
<div class="col-md-9">
<?php
	echo $error;
?>
</div>
<?php
	include('footer.php');
?>
</body>
</html>