<?php
	include('functions.php');
	redirectHTTPS();
	checkCookies();
	session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="author" content="Enrico Franco">
	<title>Book your journey</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="bootstrap/css/bootstrap.css">
</head>
<body>
<?php
	include('header.php');
	if(! checkSessionValidity()) {
		echo "<div class=\"col-md-9\">
			<div class=\"container-fluid bg-danger text-warning\"><h4>You must be logged to access this page.</h4></div>
			</div>";
		exit();
	}

	if(! bookedUser($_SESSION[$SESSION_PREFIX . 'username'])) {
		echo "<div class=\"col-md-9\">
			<div class=\"container-fluid bg-danger text-warning\"><h4>No trip booked yet.</h4></div>
			</div>";
		exit();
	}
?>
<div class="col-md-9">
<?php
	if(isset($_SESSION[$SESSION_PREFIX . 'justBooked']) && $_SESSION[$SESSION_PREFIX . 'justBooked']) { // Display success text only if the user has just booked
?>
	<div class="container-fluid bg-success text-info">
		<h4>You are succesfully booked.</h4>
	</div>
<?php
		$_SESSION[$SESSION_PREFIX . 'justBooked'] = false;
	}
?>
	<h2><span class="glyphicons glyphicon glyphicon-pushpin"></span> Booked journey</h2>
<?php
	$connection = connect();
	$bookings = retrievePeople($connection);

	// Retrieve departure and arrival cities for the user
	$statement = mysqli_stmt_init($connection);
	mysqli_stmt_prepare($statement, "SELECT `departure`, `arrival` FROM `booking` WHERE `username` = ?");
	mysqli_stmt_bind_param($statement, 's', $user);
	$user = $_SESSION[$SESSION_PREFIX . 'username'];
	mysqli_stmt_execute($statement);
	mysqli_stmt_bind_result($statement, $departure, $arrival);
	mysqli_stmt_fetch($statement);
	mysqli_stmt_close($statement);
	close($connection);

	// Detect cities
	$cities = array();
	$numberOfCities = 0;

	foreach($bookings as $city => $values) {
		$cities[$numberOfCities++] = $city;
	}
?>
	<div class="table-responsive">
		<table class="table">
			<tbody>
				<tr>
					<th colspan="3">Segment</th>
					<th></th>
					<th>Booked users</th>
				</tr>
<?php
	for($i = 0; $i < $numberOfCities - 1; $i++) {
		$city = $cities[$i]; // Needed to access bookings array

		// Highlight in red city if is the departure one
		if(strcmp($cities[$i], $departure) == 0) {
			$departureText = "<span class=\"text-danger\"><strong><span class=\"glyphicon glyphicon-pushpin\"></span> " . $cities[$i] . "</strong></span>";
		} else {
			$departureText = $cities[$i];
		}

		$string = "<td>" . $departureText;
		$string .= "<td><span class=\"glyphicon glyphicon-arrow-right\">";

		// Highlight in red city if is the arrival one
		if(strcmp($cities[$i+1], $arrival) == 0) {
			$arrivalText = "<span class=\"text-danger\"><strong><span class=\"glyphicon glyphicon-pushpin\"></span> " . $cities[$i+1] . "</strong></span>";
		} else {
			$arrivalText = $cities[$i+1];
		}

		$string .= "<td>" . $arrivalText;

		// Highlight the row, if the segment is within the booked trip
		if(strcmp($cities[$i], $departure) >= 0 && strcmp($cities[$i+1], $arrival) <= 0) {
			$highlightText = " class=\"bg-success\" ";
		} else {
			$highlightText = "";
		}

		echo "<tr" . $highlightText . ">" . $string; // $departureText . " " . $arrivalText; // Segment

		$numberOfPeople = $bookings[$city]['numberOfPeople'];
		if($numberOfPeople == 0) {
			$string = "<td>Nobody has already booked this trip!";
		} else if($numberOfPeople == 1) {
			$string = "<td>1 person has already booked this trip!";
		} else {
			$string = "<td>" . $numberOfPeople . " people have already booked this trip!";
		}

		$available = $GLOBALS["SHUTTLE_CAPACITY"] - $numberOfPeople;

		$string .= " " . ($GLOBALS["SHUTTLE_CAPACITY"] - $numberOfPeople) . " place";
		$string .= ($available != 1 ? "s are" : " is") . " still available.";

		echo $string; // People and availability

		// User correctly logged in, show other booked users
		$string = "<td>Booked users: ";
		if($numberOfPeople > 0) {
			foreach($bookings[$city]['users'] as $index => $user) {
				$people = $bookings[$city]['people'][$index];
				if($user == $_SESSION[$SESSION_PREFIX . 'username']) {
					$string .= "<strong><span class=\"text-danger\">" . $user . " (" . $people . ($people > 1 ? " places" : " place") . ")</span></strong>, ";
				} else {
					$string .= $user . " (" . $people . ($people > 1 ? " places" : " place") . "), ";
				}
			}
			$string = substr($string, 0, -2);
		} else {
			$string .= "None";
		}

		echo $string; // Booked users
	}
?>
			</tbody>
		</table>
	</div> <!-- End of table -->
	<div class="container-fluid">
		<a href="deletebooking.php" class="btn btn-danger">
			<span class="glyphicon glyphicon-trash"></span>
			Delete my booking
		</a>
	</div> <!-- End of button -->
</div> <!-- End of content -->
<?php
	include('footer.php');
?>
<script type="text/javascript"><!--
	document.getElementById("mybooking").className = "active";
//--></script>
</body>
</html>