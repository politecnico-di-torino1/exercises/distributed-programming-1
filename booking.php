<?php
	include('functions.php');
	redirectHTTPS();
	checkCookies();
	session_start();

	// If the user is not authorized, prepare an error message
	if(! checkSessionValidity()) {
		$error = "<div class=\"container-fluid bg-danger text-warning\"><h4>You must be logged to access this page.</h4></div>";
		$fatalError = true;
		goto displaypage;
	}

	// If the user is already booked, prepare an error message
	if(bookedUser($_SESSION[$SESSION_PREFIX . 'username'])) {
		$error = "<div class=\"container-fluid bg-danger text-warning\"><h4>You already have an active booking. Please, delete it before making another one.</h4></div>";
		$fatalError = true;
		goto displaypage;
	}

	if(isset($_REQUEST['submit'])) {
		if(isset($_REQUEST['departures']) && isset($_REQUEST['arrivals'])
			&& isset($_REQUEST['passengers']) && is_numeric($_REQUEST['passengers'])) {
			$connection = connect();

			// Sanitize departure city
			$departure = sanitizeString($_REQUEST['departures']);
			$departure = mysqli_escape_string($connection, $departure);
			// Next two lines necessary if JS is disabled
			$departure = trim($departure);
			$departure = strtoupper($departure);

			// Sanitize arrival city
			$arrival = sanitizeString($_REQUEST['arrivals']);
			$arrival = mysqli_escape_string($connection, $arrival);
			// Next two lines necessary if JS is disabled
			$arrival = trim($arrival);
			$arrival = strtoupper($arrival);

			// If here, it is numeric and the integer part is considered
			// Consider the absolute part to avoid possible requests with negative numbers submitted using GET method
			$passengers = (int) abs($_REQUEST['passengers']);

			if(strcmp($departure, $arrival) < 0) {
				// Strings are correctly ordered
				mysqli_autocommit($connection, false);
				mysqli_begin_transaction($connection);

				$bookings = retrievePeople($connection);

				// Detect cities
				$cities = array();
				$numberOfCities = 0;
				foreach($bookings as $city => $values) {
					$cities[$numberOfCities++] = $city;
				}

				// Detect the first city to consider
				for($i = 0; $i < $numberOfCities; $i++) {
					$city = $cities[$i];
					if(strcmp($city, $departure) > 0) {
						break;
					}
				}
				$indexStartingCity = $i - 1;

				// Detect the last city to consider
				for(; $i < $numberOfCities; $i++) {
					$city = $cities[$i];
					if(strcmp($city, $arrival) > 0) {
						break;
					}
				}
				$indexEndingCity = $i - 1;

				// Index adjustments
				if($indexStartingCity < 0) {
					// Departure is before the current first city
					if($indexEndingCity >= 0) {
						// Arrival is not before the current first city
						// It is needed to check the cities, starting from the first
						$indexStartingCity = 0;
					}
					// If $indexEndingCity < 0, booking is OK
				}

				// For the intermediate cities, verify if there are enough available places
				$bookingOk = checkAvailability($bookings, $cities, $indexStartingCity, $indexEndingCity, $passengers);

				if($bookingOk) {
					$stmtSelect = mysqli_stmt_init($connection);
					mysqli_stmt_prepare($stmtSelect, "SELECT * FROM `shuttle` WHERE `city` = ?"); // Be sure that city does not exist yet
					mysqli_stmt_bind_param($stmtSelect, 's', $city);

					// Check if departure city already exists
					$city = $departure;
					if(! mysqli_stmt_execute($stmtSelect))
						throw new Exception();
					mysqli_stmt_store_result($stmtSelect);
					$departureCounter = mysqli_stmt_num_rows($stmtSelect);
					mysqli_stmt_free_result($stmtSelect);

					// Check if arrival city already exists
					$city = $arrival;
					if(! mysqli_stmt_execute($stmtSelect))
						throw new Exception();
					mysqli_stmt_store_result($stmtSelect);
					$arrivalCounter = mysqli_stmt_num_rows($stmtSelect);
					mysqli_stmt_free_result($stmtSelect);
					mysqli_stmt_close($stmtSelect);

					try {
						$stmtInsert = mysqli_stmt_init($connection);
						mysqli_stmt_prepare($stmtInsert, "INSERT INTO `shuttle` (`city`) VALUES (?)");
						mysqli_stmt_bind_param($stmtInsert, 's', $city);

						if($departureCounter == 0) {
							// Departure city must be inserted
							$city = $departure;
							if(! mysqli_stmt_execute($stmtInsert))
								throw new Exception();
						}

						if($arrivalCounter == 0) {
							// Arrival city must be inserted
							$city = $arrival;
							if(! mysqli_stmt_execute($stmtInsert))
								throw new Exception();
						}

						mysqli_stmt_close($stmtInsert);

						$stmtInsert = mysqli_stmt_init($connection);
						mysqli_stmt_prepare($stmtInsert, "INSERT INTO `booking` (`username`, `departure`, `arrival`, `people`) VALUES (?, ?, ?, ?)");
						mysqli_stmt_bind_param($stmtInsert, 'sssi', $_SESSION[$SESSION_PREFIX . 'username'], $departure, $arrival, $passengers);

						if(! mysqli_stmt_execute($stmtInsert))
							throw new Exception();

						mysqli_stmt_close($stmtInsert);
						mysqli_commit($connection);
						close($connection);

						$_SESSION[$SESSION_PREFIX . 'justBooked'] = true; // Needed to display only once the success text
						header("location: mybooking.php");
						exit();
					} catch(Exception $e) {
						// Some error occurred
						mysqli_rollback($connection);
						$error = "<div class=\"container-fluid bg-danger text-warning\"><h4>Some error occurred! Please, retry.</h4></div>";
					}
				} else {
					$error = "<div class=\"container-fluid bg-danger text-warning\"><h4>Unable to book! Please, retry.</h4></div>";
					mysqli_rollback($connection);
				}
			} else {
				$error = "<div class=\"container-fluid bg-danger text-warning\"><h4>Arrival must strictly follow departure in alphabetic order! Please, retry.</h4></div>";
			}
			mysqli_autocommit($connection, true);
			close($connection);
		} else {
			$error = "<div class=\"container-fluid bg-danger text-warning\"><h4>Please, complete all fields.</h4></div>";
		}
	}

	displaypage: // Label
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="author" content="Enrico Franco">
	<title>Book your journey</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="bootstrap/css/bootstrap.css">
</head>
<body>
<?php
	include('header.php');
?>
<div class="col-md-9">
<?php
	if(isset($error))
		echo $error;
	if(isset($fatalError)) // In case of "fatal" error, i.e., unvalid session or user already booked, interrupt the page loading
		exit();
?>
	<h2><span class="glyphicons glyphicon glyphicon-pushpin"></span> Book your journey</h2>
	<form class="form-horizontal" method="post" action="booking.php">
		<div class="form-group">
			<label class="control-label col-sm-2" for="departures">Departure:</label>
			<div class="col-sm-10">
				<input list="departures" class="form-control" name="departures" placeholder="Departure city" required="required" id="departures_input">
				<datalist id="departures">
<?php
	$connection = connect();
	$result = query($connection, "SELECT `city` FROM `shuttle` ORDER BY `city`");
	$rows = mysqli_num_rows($result);
	for($i = 0; $i < $rows; $i++) {
		$row = mysqli_fetch_array($result, MYSQLI_ASSOC);
		echo "<option value =\"" . $row['city'] . "\">";
	}
?>
				</datalist>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-2" for="arrivals">Arrival:</label>
			<div class="col-sm-10">
				<input list="arrivals" class="form-control" name="arrivals" placeholder="Arrival city" required="required" id="arrivals_input">
				<datalist id="arrivals">
<?php
	// Reset pointer from the beginning
	mysqli_data_seek($result, 0);
	for($i = 0; $i < $rows; $i++) {
		$row = mysqli_fetch_array($result, MYSQLI_ASSOC);
		echo "<option value =\"" . $row['city'] . "\">";
	}
	mysqli_free_result($result);
	close($connection);
?>
				</datalist>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-2" for="passengers">Number of passengers:</label>
			<div class="col-sm-10">
				<select class="form-control" id="passengers" name="passengers" required="required">
<?php
	for($i = 1; $i <= $GLOBALS['SHUTTLE_CAPACITY']; $i++) {
		echo "<option>$i</option>";
	}
?>
				</select>
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
				<button type="reset" class="btn btn-danger">Cancel</button>
				<button type="submit" class="btn btn-success" name="submit">Submit</button>
			</div>
		</div>
	</form>
</div>
<?php
	include('footer.php');
?>
<script type="text/javascript"><!--
var arrival = document.getElementById("arrivals_input");
var departure = document.getElementById("departures_input");

function validatePath() {
	var textArrival = arrival.value.trim().toUpperCase();
	var textDeparture = departure.value.trim().toUpperCase();

	if(textDeparture.localeCompare(textArrival) >= 0) {
		arrival.setCustomValidity("Arrival must strictly follow departure in alphabetic order!");
	} else {
		arrival.setCustomValidity("");
	}
}

arrival.oninput = validatePath;
departure.oninput = validatePath;

function trimString() {
	arrival.value = arrival.value.trim().toUpperCase();
	departure.value = departure.value.trim().toUpperCase();
}

arrival.onblur = trimString;
departure.onblur = trimString;

//--></script>
<script type="text/javascript"><!--
	document.getElementById("book").className = "active";
//--></script>
</body>
</html>